import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_default(host):
    local_vars = host.ansible.get_variables()
    filename = local_vars["dns_client_resolv_conf_file"]
    resolv = host.file(filename)
    for dns in local_vars["dns_client_servers"]:
        line = "nameserver {}".format(dns)
        assert resolv.contains(line)
    assert resolv.contains("search {}".format(local_vars["dns_client_suffixes"]))
    assert resolv.contains("options {}".format(' '.join(local_vars["dns_client_options"])))
