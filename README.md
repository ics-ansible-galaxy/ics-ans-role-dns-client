# ics-ans-role-dns-client

Ansible role to configure dns resolution on a linux client.

## Role Variables

```yaml
dns_client_servers:
  - 8.8.8.8
  - 1.1.1.1
dns_client_suffixes:
  - tn.esss.lu.se
  - esss.lu.se
dns_client_options:
  - rotate
  - timeout:1
  - retries:1
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-dns-client
```

## License

BSD 2-clause
